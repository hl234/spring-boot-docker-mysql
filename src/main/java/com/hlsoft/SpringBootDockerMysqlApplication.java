package com.hlsoft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注释信息
 */
@SpringBootApplication
@RestController
public class SpringBootDockerMysqlApplication {

	@Autowired
	private PersonRepository repository;

	@RequestMapping("/")
	public String home(){
		Person p = this.repository.findAll().iterator().next();
		return "Hello " + p.getName() + "!";
	}


	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerMysqlApplication.class, args);
	}
}
