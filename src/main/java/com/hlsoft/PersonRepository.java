package com.hlsoft;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lei on 2017/5/13.
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Long>{
}
